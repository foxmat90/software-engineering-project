public class Professionista extends UtenteRegistrato {
	private String categoria;
	private String partitaIva;
	private String indirizzo;
	private String telefono;
	
	public Professionista(String username, String password, String nome, String cognome, String email,
							String categoria, String partitaIVa, String indirizzo, String telefono){
		super(username,password,nome,cognome,email);
		this.categoria = categoria;
		this.partitaIva = partitaIva;
		this.indirizzo = indirizzo;
		this.telefono = telefono;
	}
	
	public void modificaInformazioniAccount(String username, String password, String nome, String cognome, String email,
							String categoria, String partitaIVa, String indirizzo, String telefono){
		   super.modificaInformazioniAccount(username, password, nome, cognome, email);
		   this.setCategoria(categoria);
		   this.setPartitaIva(partitaIva);
		   this.setIndirizzo(indirizzo);
		   this.setTelefono(telefono);
								
	}
	
	public void setCategoria(categoria){
		this.categoria = categoria;
	}
	
	public void setPartitaIva(partitaIva){
		this.partitaIva = partitaIva;
	}
	
	public void setIndirizzo(indirizzo){
		this.indirizzo = indirizzo;
	}
	
	public void setTelefono(telefono){
		this.telefono = telefono;	
	}
	
	public String getCategoria(){
		return this.categoria;
	}
	
	public String getPartitaIva(){
		return this.partitaIva;
	}
	
	public String getIndirizzo(){
		return this.indirizzo;
	}
	
	public String getTelefono(){
		return this.telefono;
	}
		
		
	
	
	
	
	
}