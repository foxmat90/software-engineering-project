public class Appuntamento {

	private String data;
	private Professionista professionista;
	private Utente utente;
	
	public Appuntamento(String data, Professionista professionista, Utente utente){
		this.data = data;
		this.professionista = professionista;
		this.utente = utente;
	}

}