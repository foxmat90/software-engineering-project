public class Messaggio {
	private UtenteRegistrato mittente;
	private UtenteRegistrato destinatario;
	private String testoMessaggio;
	
	public Messaggio(UtenteRegistrato mittente, UtenteRegistrato destinatario, String testoMessaggio){
		this.mittente = mittente;
		this.destinatario = destinatario;
		this.testoMessaggio = testoMessaggio;
		
	}
	
	public UtenteRegistrato getMittente(){
		return this.mittente;
	}
	
	public UtenteRegistrato getDestinatario(){
		return this.destinatario;
	}
	
	
}