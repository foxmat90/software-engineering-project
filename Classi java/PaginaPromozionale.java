public class PaginaPromozionale {
	private Integer idPagina;
	private Professionista professionista;
	private Recensione[] recensioni;
	private String contenuto;
	
	public PaginaPromozionale(Integer idPagina, Professionista professionista, Recensione[] recensioni, String contenuto){
		this.idPagina = idPagina;
		this.professionista = professionista;
		this.recensioni = recensioni;
		this.contenuto = contenuto;
	}
	
	public void modificaPaginaPersonale(String contenuto){
		this.contenuto = contenuto;
	} 
	
	
	
}