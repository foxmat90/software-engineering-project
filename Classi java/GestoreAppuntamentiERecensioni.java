public class GestoreAppuntamentiERecensioni {
	
	public GestoreAppuntamentiERecensioni(){
		
		
	}
	
	public void richiediAppuntamento(Professionista professionista){
		
	}
	
	public Appuntamento confermaAppuntamento(Appuntamento appuntamento, boolean risposta){
		if (risposta == true){
			
			// notifica utente di conferma appuntamento
			return appuntamento;
		}
		else {
			// notifica utente di rifiuto appuntamento
			// elimina appuntamento dal database appuntamenti
			appuntamento = null;
			return appuntamento;
		}
	}
	
	public void inserisciRecensione(Recensione r){
		// permette all'utente di 
		// creare una recensione nel database
		
	}
	
	public void modificaRecensione(Recensione r){
		// permette all'utente di 
		// modifcare una sua contenuto di una recensione presente nel database
	}
	
	public void cancellaRecensione(Recensione r){
		// permette all'amministratore di
		// cancellare una recensione dal database
	}
	
	
	
}