public class utenteRegistrato {
	
	private String username;
	private String password;
	private String nome;
	private String cognome;
	private String email;
	private boolean abilitato;

	public utenteRegistrato(String username, String password, String nome, String cognome, String email){
		this.username = username;
		this.password = password;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.abilitato = true;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}	

	public void setCognome(String cognome){
		this.cognome = cognome;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public void setAbilitato(boolean abilitato){
		this.abilitato = abilitato;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public String getNome(){
		return this.nome;
	}	

	public String getCognome(){
		return this.cognome;
	}

	public String getEmail(){
		return this.email;
	}

	public boolean getAbilitato(){
		return this.abilitato;
	}
	
	public void modificaInformazioniAccount(String username, String password, String nome, String cognome, String email){
		this.setUsername(username);
		this.setPassword(password);
		this.setNome(nome);
		this.setCognome(cognome);
		this.setEmail(email);
	}
}