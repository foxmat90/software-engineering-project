public class Recensione {
	private Integer idRecensione;
	private String testoRecensione;
	private Utente autore;
	private Professionista professionista;
	
	public Recensione(Integer idRecensione, String testoRecensione, Utente autore, Professionista professionista){
		this.idRecensione=idRecensione;
		this.testoRecensione = testoRecensione;
		this.autore = autore;
		this.professionista = professionista;
	}
	
	public void setTestoRecensione(String testoRecensione){
		this.testoRecensione = testoRecensione;	
	}
	
	
	
	
}